const getUpdatedWord = (word) => {
    const isShared = word.indexOf('>') > -1;
    return {
        word: isShared ? word.replace('>', '').trim() : word,
        isShared
    }
}

export default getUpdatedWord;