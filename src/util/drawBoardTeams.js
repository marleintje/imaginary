import drawBorder from "./drawBorder";

const drawBoardTeams = (ctx, squareSize, squareBorderWidth, settingsContext) => {
    console.log(settingsContext)
    ctx.save()
    const rectWidth = squareSize * 2;
    const rectHeight = squareSize * .5;

    // Box for text "TEAM X"
    const teamBoxCoordinates = [
        { x: 1.5, y: 1.5, text: 'Team A' },
        { x: 7.5, y: 1.5, text: 'Team B' },
        { x: 1.5, y: 7.5, text: 'Team C' },
        { x: 7.5, y: 7.5, text: 'Team D' },
    ].slice(0, settingsContext.numberOfTeams)

    teamBoxCoordinates.forEach(({ x, y }) => {
        drawBorder(ctx, x * squareSize, y * squareSize, rectWidth, rectHeight, squareBorderWidth * 0.6)
    })

    ctx.fillStyle = '#FFF';

    teamBoxCoordinates.forEach(multiplier => {
        ctx.fillRect(multiplier.x * squareSize, multiplier.y * squareSize, rectWidth, rectHeight);
    })

    ctx.fillStyle = '#000';
    ctx.font = (`${squareSize * .4}px Arial`)

    // Text "TEAM X"
    teamBoxCoordinates.forEach(({ x, y, text }) => {
        const metrics = ctx.measureText(text);
        const textWidth = metrics.width;
        const textHeight = metrics.actualBoundingBoxAscent + metrics.actualBoundingBoxDescent;
        ctx.fillText(
            text,
            x * squareSize + (rectWidth - textWidth) / 2,
            y * squareSize + rectHeight - (rectHeight - textHeight) / 2
        )
    })

    // Button circles
    const playerGreen = '#1c995c'; // Original #32bd7a
    const playerBlue = '#2980bd'; // Original #72bcef
    const playerRed = '#d32f18'; // Original #f06f5d
    const playerYellow = '#e9e507'; // Original #fdf92a

    const buttonCircleCoordinates = [
        { centerX: 2, centerY: 2.55, fillStyle: playerGreen },
        { centerX: 3, centerY: 2.55, fillStyle: playerGreen },
        { centerX: 8, centerY: 2.55, fillStyle: playerBlue },
        { centerX: 9, centerY: 2.55, fillStyle: playerBlue },
        { centerX: 2, centerY: 8.55, fillStyle: playerRed },
        { centerX: 3, centerY: 8.55, fillStyle: playerRed },
        { centerX: 8, centerY: 8.55, fillStyle: playerYellow },
        { centerX: 9, centerY: 8.55, fillStyle: playerYellow },
    ].slice(0, settingsContext.numberOfTeams * 2)

    // + and - sign for buttons
    buttonCircleCoordinates.forEach(({centerX, centerY, fillStyle}, index) => {
        ctx.beginPath();
        ctx.arc(squareSize * centerX, squareSize * centerY, squareSize * 0.38 , 0, 2 * Math.PI);
        ctx.fillStyle = fillStyle;
        ctx.fill();
        ctx.lineWidth = 5;
        ctx.stroke();
        ctx.fillStyle = '#000'
        ctx.fillRect(
            centerX * squareSize - squareSize * 0.25,
            centerY * squareSize - squareSize * 0.05,
            squareSize * 0.5,
            squareSize * 0.1
        );
        if (index % 2) ctx.fillRect(
            centerX * squareSize - squareSize * 0.05,
            centerY * squareSize - squareSize * 0.25,
            squareSize * 0.1,
            squareSize * 0.5
        );
    })

    ctx.closePath();
    ctx.restore()
}

export default drawBoardTeams;
