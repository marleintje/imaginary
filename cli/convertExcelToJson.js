const fs = require('fs');

// Create an instance for XlsParser
var parser = new (require('simple-excel-to-json').XlsParser)();
var doc = parser.parseXls2Json('./cli/Imaginary.xlsx');

// Data of first sheet converted into array of arrays
const words = doc[0]
    .map(wordGroup => Object.values(wordGroup))
    .filter(wordGroup => wordGroup[0])

fs.writeFileSync(
    'src/data/generated-words.json',
    JSON.stringify(words),
    'utf8',
)