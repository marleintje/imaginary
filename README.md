# Imaginary

This repo serves to:
- Play Imaginary online -- https://imaginary-cards.herokuapp.com/
- In Google Chrome, when hitting `print` (CTRL + P), it exactly fits and is centered on A4 format, to send to a print shop
- Create cards with words that are suitable for drawing games.
- Easily extract words from an excel.xlsx file and output to JSON, to use as input for the cards

## Development
- `npm i`
- `npm start` runs the app in development mode
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Steps to generate new cards
- Create a file `cli/Imaginary.xlsx` (the cli folder already exists, but the excel file is gitignored)
    - Add 5 words per row.
    - If you wish the word to be prepended by ▶ in the final result, then prepend it with a greater than `>` symbol in the Excel file
    - Example of 2 excel file entries:

|   |   |   |   |   |
|---|---|---|---|---|
| >Voorzitter | >Hoge hoed |  Sproeien |  Teugels |  Eiffeltoren |
| >Tortelduif | >Bikini | Ouwehoeren | Lawaaierig | Nucleair |

- `npm run getwords` (output the new words to `/data/generated-words.json`)
- `npm start` in a Chrome browser, now `CTRL + P` (destination `Save as PDF`). Each A4 should contain 9 cards.
